@extends('layouts.master')
@section('title', 'Admin')

@section('content')

<link rel="stylesheet" href="{{ secure_asset('css/admin.css') }}" type="text/css">

    <div id="admin">
        <div onclick="location.href='http://p4sarahvrielinck-neonglowstix.c9users.io/Product'">
          <p>Product</p>
        </div>
        <div onclick="location.href='{{ action('SupplierController@index') }}'">
            <p>Supplier</p>
        </div>
        <div onclick="location.href='http://p4sarahvrielinck-neonglowstix.c9users.io/Category'">
           <p>Categories</p>
        </div>
        
        <div onclick="location.href='{{ action('CustomerController@index') }}'">
            <p>Customer</p>
        </div>
        <div>
           
        </div>
        <div>
            <p>Order (n/a)</p>
        </div>
        <div>
            <p>Order Items (n/a)</p>
        </div>
        <div>
            
        </div>
        <div onclick="location.href='{{ action('CountryController@index') }}'">
            <p>Country</p>
        </div>
        <div onclick="location.href='http://p4sarahvrielinck-neonglowstix.c9users.io/OrderStatus'">
            <p>Order Status</p>
        </div>
        <div onclick="location.href='http://p4sarahvrielinck-neonglowstix.c9users.io/UnitBase'">
            <p>Unit Base</p>
        </div>
    </div>
@endsection
@extends('layouts.master')
@section('title', 'Edit Country')

@section('content')



@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;" >Edit Country</h2>
            <form method="POST" action="{{ action('CountryController@update', $country->id) }}">
                <input type="hidden" name="_method" value="PUT" />  
                {{ csrf_field() }}
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CountryController@index')  }}">Cancel</a>
                <button class="btn btn-primary" style="float:right; margin-top: 17px; margin-right: 5px;">Update</button>
                
                <div class="form-group" style="clear:both;">
                    <label for="Code">Code:</label> <span style="color:red;">*</span>
                    <input type="text" name="Code" class="form-control" id="Code" value="{{ $country->Code }}" />
                </div>
                <div class="form-group">
                    <label for="Name">Name:</label> <span style="color:red;">*</span>
                    <input type="text" name="Name" class="form-control" id="Name" value="{{ $country->Name }}" />
                </div>
                <div class="form-group">
                    <label for="Latitude">Latitude:</label> 
                    <input type="text" name="Latitude" class="form-control" id="Latitude" value="{{ $country->Latitude }}"/>
                </div>
                <div class="form-group">
                    <label for="Longitude">Longitude:</label> 
                    <input type="text" name="Longitude" class="form-control" id="Longitude" value="{{ $country->Longitude }}"/>
                </div>
                
                <div class="form-group">
                    <label for="ShippingCostMultiplier">ShippingCostMultiplier:</label> 
                    <input type="text" name="ShippingCostMultiplier" class="form-control" id="ShippingCostMultiplier" value="{{ $country->ShippingCostMultiplier }}"/>
                </div>
            </form>
    </div>
</div>
    
<div class="col-md-4">
    <h3>Countries</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($countrys as $country)
                    <tr>
                        <td><a href="{{ action('CountryController@show', $country) }}">Select</a></td>
                        <td>{{ $country->Name }}</td>
                        <td>{{ $country->Code }}</td>
                    </tr>
                @endforeach
                
                {{ $countrys->links() }}
            </tbody>
        </table>
    </div>
</div>

@endsection
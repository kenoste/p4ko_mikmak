@extends('layouts.master')
@section('title', 'Country')

@section('content')

<div class="col-lg-8">
    
    <div class="col-lg-12 row">
            <h2 style="float:left;" class="hidden-xs">View Country</h2>
            <form method="POST" action="{{ action('CountryController@destroy', $country->id) }}">
                <input type="hidden" name="_method" value="DELETE"/>    
                {{ csrf_field() }}
                <a style="float:right; margin-top: 17px;" class="btn btn-primary" href="{{ action('CountryController@edit', $country)  }}">Edit</a>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CountryController@create')  }}">Insert</a>
                <button class="btn btn-danger" style="float:right; margin-top: 17px; margin-right: 5px;">Delete Country</button>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CountryController@index')  }}">Cancel</a>
            </form>
    
          
        
            <div class="form-group"  style="clear:both">
                <label for="Code">Code:</label> 
                <input type="text" name="Code" class="form-control" id="Code" value="{{ $country->Code }}" disabled>
            </div>
            <div class="form-group">
                <label for="Name">Name:</label> 
                <input type="text" name="Name" class="form-control" id="Name" value="{{ $country->Name }}" disabled/>
            </div>
            <div class="form-group">
                <label for="Latitude">Latitude:</label> 
                <input type="text" name="Latitude" class="form-control" id="Latitude" value="{{ $country->Latitude }}" disabled/>
            </div>
            <div class="form-group">
                <label for="Longitude">Longitude:</label> 
                <input type="text" name="Longitude" class="form-control" id="Longitude" value="{{ $country->Longitude }}" disabled/>
            </div>
            
            <div class="form-group">
                <label for="ShippingCostMultiplier">ShippingCostMultiplier:</label> 
                <input type="text" name="ShippingCostMultiplier" class="form-control" id="ShippingCostMultiplier" value="{{ $country->ShippingCostMultiplier }}" disabled/>
            </div>
            
            
    </div>
</div>

<div class="col-md-4">
    <h3>Countries</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($countrys as $country)
                    <tr>
                        <td><a href="{{ action('CountryController@show', $country) }}">Select</a></td>
                        <td>{{ $country->Name }}</td>
                        <td>{{ $country->Code }}</td>
                    </tr>
                @endforeach
                
                {{ $countrys->links() }}
            </tbody>
        </table>
    </div>
</div>



@endsection
@extends('layouts.master')
@section('title', 'Countries')

@section('content')

<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;">View Countries</h2>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CountryController@create')  }}">Insert</a>
    </div>
</div>
    
<div class="col-md-4">
    <h3>Countries</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($countrys as $country)
                    <tr>
                        <td><a href="{{ action('CountryController@show', $country) }}">Select</a></td>
                        <td>{{ $country->Name }}</td>
                        <td>{{ $country->Code }}</td>
                    </tr>
                @endforeach
                
                {{ $countrys->links() }}
            </tbody>
        </table>
    </div>
</div>

@endsection
@extends('layouts.master')
@section('title', 'Create Country')

@section('content')



@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-lg-8">
    <div class="col-lg-12 row">
        <form method="post"  action="{{ action('CountryController@store') }}">
            <h2 style="float:left;">Create Country</h2>
            <a style="float:right; margin-top: 2em;" class="btn btn-warning" href="{{ action('CountryController@index')}}">Cancel</a>
            <button style="float:right; margin-top: 2em; margin-right: 5px;" class="btn btn-primary" type="submit">Save</button>
            {{ csrf_field() }}
    
        <div class="form-group" style="clear:both;">
            <label for="Code">Code:</label> <span style="color:red;">*</span>
            <input type="text" name="Code" class="form-control" id="Code" value="{{ old('Code') }}"/>
        </div>
        <div class="form-group">
            <label for="Name">Name:</label> <span style="color:red;">*</span>
            <input type="text" name="Name" class="form-control" id="Name" value="{{ old('Name') }}"/>
        </div>
        <div class="form-group">
            <label for="Latitude">Latitude:</label> 
            <input type="text" name="Latitude" class="form-control" id="Latitude" value="{{ old('Latitude') }}"/>
        </div>
        <div class="form-group">
            <label for="Longitude">Longitude:</label> 
            <input type="text" name="Longitude" class="form-control" id="Longitude" value="{{ old('Longitude') }}"/>
        </div>
        
        <div class="form-group">
            <label for="ShippingCostMultiplier">ShippingCostMultiplier:</label> 
            <input type="text" name="ShippingCostMultiplier" class="form-control" id="ShippingCostMultiplier" value="{{ old('ShippingCostMultiplier') }}">
        </div>
            
        </form>
    </div>
</div>

<div class="col-md-4">
    <h3>Countries</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($countrys as $cntry)
                    <tr>
                        <td><a href="{{ action('CountryController@show', $cntry) }}">Select</a></td>
                        <td>{{ $cntry->Name }}</td>
                        <td>{{ $cntry->Code }}</td>
                    </tr>
                @endforeach
                
                 {{ $countrys->links() }}
            </tbody>
        </table>
    </div>
</div>

@endsection
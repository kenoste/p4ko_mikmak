<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mikmak - @yield('title')</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php echo asset('css/admin.css')?>" type="text/css"> 
</head>

<body>
    <header class="navbar navbar-fixed-top navbar-default">
        <div class="navbar-inner">
            <div class="container">
                <div class="page-header">
                    <h2 style="text-align: right;">MikMak <small><a href="{{ action('AdminController@index') }}">Administration</a></small></h2>
                </div>
            </div>
        </div>
    </header>
    
    <div class="container" style="margin-top: 8em; padding-bottom: 5em;">
        <main class="row">
            @yield('content')
        </main>
    </div>
    
    <footer class="navbar-fixed-bottom navbar-default navbar text-center">
        <p style="margin-top: 1em;"><a href="{{ action('AdminController@index') }}">admin panel</a> &copy; 2017 - Sarah & Ken</p>
    </footer>
</body>

</html>
 
@extends('layouts.master')
@section('title', 'Create Supplier')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-lg-8">
    <div class="col-lg-12 row">
        <form name="newCountry" method="post" action="{{ action('SupplierController@store') }}">
            <h2 style="float:left;">Create Supplier</h2>
            <a style="float:right; margin-top: 2em;" class="btn btn-warning" href="{{ action('SupplierController@index')}}">Cancel</a>
            <button style="float:right; margin-top: 2em; margin-right: 5px;" class="btn btn-primary" type="submit">Save</button>
            {{ csrf_field() }}
    
        <div class="form-group" style="clear:both;">
            <label for="Name">Name:</label><span style="color:red;">*</span> 
            <input type="text" name="Name" class="form-control" id="Name" value="{{ old('Name') }}"/>
        </div>
        <div class="form-group" style="clear:both;">
            <label for="Code">Code:</label> <span style="color:red;">*</span>
            <input type="text" name="Code" class="form-control" id="Code" value="{{ old('Code') }}"/>
        </div>
        <div class="form-group">
            <label for="Contact">Contact:</label> 
            <input type="text" name="Contact" class="form-control" id="Contact" value="{{ old('Contact') }}"/>
        </div>
        <div class="form-group">
            <label for="Address">Address:</label> 
            <input type="text" name="Address" class="form-control" id="Address" value="{{ old('Address') }}"/>
        </div>
         <div class="form-group">
            <label for="City">City:</label> 
            <input type="text" name="City" class="form-control" id="City" value="{{ old('City') }}"/>
        </div>
         <div class="form-group">
            <label for="Region">Region:</label> 
            <input type="text" name="Region" class="form-control" id="Region" value="{{ old('Region') }}"/>
        </div>
         <div class="form-group">
            <label for="PostalCode">PostalCode:</label> 
            <input type="text" name="PostalCode" class="form-control" id="PostalCode"  value="{{ old('PostalCode') }}"/>
        </div>
         <div class="form-group">
            <label for="Phone">Phone:</label> 
            <input type="text" name="Phone" class="form-control" id="Phone" value="{{ old('Phone') }}"/>
        </div>
         <div class="form-group">
            <label for="Mobile">Mobile:</label> 
            <input type="text" name="Mobile" class="form-control" id="Mobile" value="{{ old('Mobile') }}"/>
        </div>
        <div class="form-group">
            <label for="country_id">Country:</label>
            <select name="country_id" id="country_id" class="form-control" >
            @foreach ($countrys as $country) 
             
                  <option value="{{ $country->id }}">{{ $country->Name }}</option>
               
            @endforeach
            </select>
        </div>
            
            
        </form>
        </form>
    </div>
  
</div>

<div class="col-md-4">
    <h3>Supplier</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($suppliers as $supp)
                    <tr>
                        <td><a href="{{ action('SupplierController@show', $supp) }}">Select</a></td>
                        <td>{{ $supp->Name }}</td>
                        <td>{{ $supp->Code }}</td>
                    </tr>
                @endforeach
                
                {{ $suppliers->links() }}
            </tbody>
        </table>
    </div>
</div>

<script>
    var sel = document.getElementById('country_id');
    sel.value = {{ old('country_id') }}
</script>


@endsection
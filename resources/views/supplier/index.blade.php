@extends('layouts.master')
@section('title', 'Supplier')

@section('content')

<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;">View Suppliers</h2>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('SupplierController@create')  }}">Insert</a>
    </div>
</div>
    
<div class="col-md-4">
    <h3>Supplier</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($suppliers as $supp)
                    <tr>
                        <td><a href="{{ action('SupplierController@show', $supp) }}">Select</a></td>
                        <td>{{ $supp->Name }}</td>
                        <td>{{ $supp->Code }}</td>
                    </tr>
                @endforeach
                
                 {{ $suppliers->links() }}
            </tbody>
        </table>
    </div>
</div>

@endsection
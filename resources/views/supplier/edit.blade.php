@extends('layouts.master')
@section('title', 'Edit Supplier')

@section('content')


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;">Edit Supplier</h2>
            <form method="POST" action="{{ action('SupplierController@update', $supplier->id) }}">
                <input type="hidden" name="_method" value="PUT" />  
                {{ csrf_field() }}
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('SupplierController@index')  }}">Cancel</a>
                <button class="btn btn-primary" style="float:right; margin-top: 17px; margin-right: 5px;">Update</button>
                
            
                <div class="form-group" style="clear:both;">
                    <label for="Name">Name:</label> <span style="color:red;">*</span>
                    <input type="text" name="Name" class="form-control" id="Name" value="{{ $supplier->Name }}" />
                </div>
                <div class="form-group" style="clear:both;">
                    <label for="Code">Code:</label> <span style="color:red;">*</span>
                    <input type="text" name="Code" class="form-control" id="Code" value="{{ $supplier->Code }}" />
                </div>
                <div class="form-group">
                    <label for="Contact">Contact:</label> 
                    <input type="text" name="Contact" class="form-control" id="Contact" value="{{ $supplier->Contact }}" />
                </div>
                <div class="form-group">
                    <label for="Address">Address:</label> 
                    <input type="text" name="Address" class="form-control" id="Address" value="{{ $supplier->Address }}" />
                </div>
                 <div class="form-group">
                    <label for="City">City:</label> 
                    <input type="text" name="City" class="form-control" id="City" value="{{ $supplier->City }}" />
                </div>
                 <div class="form-group">
                    <label for="Region">Region:</label> 
                    <input type="text" name="Region" class="form-control" id="Region" value="{{ $supplier->Region }}" />
                </div>
                 <div class="form-group">
                    <label for="PostalCode">PostalCode:</label> 
                    <input type="text" name="PostalCode" class="form-control" id="PostalCode" value="{{ $supplier->PostalCode }}" />
                </div>
                 <div class="form-group">
                    <label for="Phone">Phone:</label> 
                    <input type="text" name="Phone" class="form-control" id="Phone" value="{{ $supplier->Phone }}" />
                </div>
                 <div class="form-group">
                    <label for="Mobile">Mobile:</label> 
                    <input type="text" name="Mobile" class="form-control" id="Mobile" value="{{ $supplier->Mobile }}" />
                </div>
                <div class="form-group">
                    <label for="country_id">Country:</label>
                    <select name="country_id" id="country_id" class="form-control" >
                    @foreach ($countrys as $country) 
                     
                          <option value="{{ $country->id }}">{{ $country->Name }}</option>
                       
                    @endforeach
                    </select>
                </div>
                
            </form>
    </div>
   
</div>
    
<div class="col-md-4">
    <h3>Supplier</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($suppliers as $supp)
                    <tr>
                        <td><a href="{{ action('SupplierController@show', $supp) }}">Select</a></td>
                        <td>{{ $supp->Name }}</td>
                        <td>{{ $supp->Code }}</td>
                    </tr>
                @endforeach
                
                {{ $suppliers->links() }}
            </tbody>
        </table>
    </div>
</div>
    

<script>
    var sel = document.getElementById('country_id');
    sel.value = {{ $supplier->country->id }};
</script>

@endsection
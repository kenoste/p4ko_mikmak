@extends('layouts.master')
@section('title', 'Supplier')

@section('content')

<div class="col-lg-8">
    
    <div class="col-lg-12 row">
            <h2 style="float:left;" class="hidden-xs">View Supplier</h2>
            <form method="POST" action="{{ action('SupplierController@destroy', $supplier->id) }}">
                <input type="hidden" name="_method" value="DELETE"/>    
                {{ csrf_field() }}
                <a style="float:right; margin-top: 17px;" class="btn btn-primary" href="{{ action('SupplierController@edit', $supplier)  }}">Edit</a>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('SupplierController@create')  }}">Insert</a>
                <button class="btn btn-danger" style="float:right; margin-top: 17px; margin-right: 5px;">Delete Supplier</button>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('SupplierController@index')  }}">Cancel</a>
            </form>
    
          
        <div class="form-group" style="clear:both;">
            <label for="Name">Name:</label> 
            <input type="text" name="Name" class="form-control" id="Name" value="{{ $supplier->Name }}" disabled/>
        </div>
        <div class="form-group" style="clear:both;">
            <label for="Code">Code:</label> 
            <input type="text" name="Code" class="form-control" id="Code" value="{{ $supplier->Code }}" disabled/>
        </div>
        <div class="form-group">
            <label for="Contact">Contacr:</label> 
            <input type="text" name="Contact" class="form-control" id="Contact" value="{{ $supplier->Contact }}" disabled/>
        </div>
        <div class="form-group">
            <label for="Address">Address:</label> 
            <input type="text" name="Address" class="form-control" id="Address" value="{{ $supplier->Address }}" disabled/>
        </div>
         <div class="form-group">
            <label for="City">City:</label> 
            <input type="text" name="City" class="form-control" id="City" value="{{ $supplier->City }}" disabled/>
        </div>
         <div class="form-group">
            <label for="Region">Region:</label> 
            <input type="text" name="Region" class="form-control" id="Region" value="{{ $supplier->Region }}" disabled/>
        </div>
         <div class="form-group">
            <label for="PostalCode">PostalCode:</label> 
            <input type="text" name="PostalCode" class="form-control" id="PostalCode" value="{{ $supplier->PostalCode }}" disabled/>
        </div>
         <div class="form-group">
            <label for="Phone">Phone:</label> 
            <input type="text" name="Phone" class="form-control" id="Phone" value="{{ $supplier->Phone }}" disabled/>
        </div>
         <div class="form-group">
            <label for="Mobile">Mobile:</label> 
            <input type="text" name="Mobile" class="form-control" id="Mobile" value="{{ $supplier->Mobile }}" disabled/>
        </div>
        <div class="form-group">
            <label for="country_id">Country:</label>
            <select name="country_id" id="country_id" class="form-control" disabled>
            @foreach ($countrys as $country) 
             
                  <option value="{{ $country->id }}">{{ $country->Name }}</option>
               
            @endforeach
            </select>
        </div>
            
            
       
    </div>
    
</div>

<div class="col-md-4">
    <h3>Supplier</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>Name</th>
                <th>Code</th>
            </thead>
            <tbody>
                @foreach ($suppliers as $supp)
                    <tr>
                        <td><a href="{{ action('SupplierController@show', $supp) }}">Select</a></td>
                        <td>{{ $supp->Name }}</td>
                        <td>{{ $supp->Code }}</td>
                    </tr>
                @endforeach
                
                {{ $suppliers->links() }}
            </tbody>
        </table>
    </div>
</div>


<script>
    var sel = document.getElementById('country_id');
    sel.value = {{ $supplier->country->id }};
</script>

@endsection
@extends('layouts.master')
@section('title', 'Customers')

@section('content')

<div class="col-lg-8">
    <div class="col-lg-12 row">
            <h2 style="float:left;">View Customers</h2>
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CustomerController@create')  }}">Insert</a>
    </div>
</div>
    
<div class="col-md-4">
    <h3>Customers</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>LastName</th>
                <th>FirstName</th>
            </thead>
            <tbody>
                @foreach ($customers as $cust)
                    <tr>
                        <td><a href="{{ action('CustomerController@show', $cust) }}">Select</a></td>
                        <td>{{ $cust->LastName }}</td>
                        <td>{{ $cust->FirstName }}</td>
                    </tr>
                @endforeach
                
                 {{ $customers->links() }}
            </tbody>
        </table>
    </div>
</div>

@endsection
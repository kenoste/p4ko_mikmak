@extends('layouts.master')
@section('title', 'Edit Customer')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif    

    
<div class="col-lg-8">
    <div sclass="col-lg-12 row">
        <h2 style="float:left;">Edit Customer</h2>
        <form method="POST" action="{{ action('CustomerController@update', $customer->id) }}">
            <input type="hidden" name="_method" value="PUT" />  
                {{ csrf_field() }}
            <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CustomerController@index')  }}">Cancel</a>
            <button class="btn btn-primary" style="float:right; margin-top: 17px; margin-right: 5px;">Update</button>
                
            
        <div class="form-group" style="clear:both;">
            <label for="NickName">NickName:</label> <span style="color:red;">*</span>
            <input type="text" name="NickName" class="form-control" id="NickName" value="{{ $customer->NickName }}"/>
        </div>
        <div class="form-group">
            <label for="FirstName">FirstName:</label> <span style="color:red;">*</span>
            <input type="text" name="FirstName" class="form-control" id="FirstName" value="{{ $customer->FirstName }}"/>
        </div>
        <div class="form-group">
            <label for="LastName">LastName:</label> <span style="color:red;">*</span>
            <input type="text" name="LastName" class="form-control" id="LastName" value="{{ $customer->LastName }}"/>
        </div>
        <div class="form-group">
            <label for="Address1">Address1:</label> <span style="color:red;">*</span>
            <input type="text" name="Address1" class="form-control" id="Address1" value="{{ $customer->Address1 }}"/>
        </div>
        <div class="form-group">
            <label for="Address2">Address2:</label> 
            <input type="text" name="Address2" class="form-control" id="Address2" value="{{ $customer->Address2 }}"/>
        </div>
         <div class="form-group">
            <label for="City">City:</label> <span style="color:red;">*</span>
            <input type="text" name="City" class="form-control" id="City" value="{{ $customer->City }}"/>
        </div>
         <div class="form-group">
            <label for="Region">Region:</label> 
            <input type="text" name="Region" class="form-control" id="Region" value="{{ $customer->Region }}"/>
        </div>
         <div class="form-group">
            <label for="PostalCode">PostalCode:</label> <span style="color:red;">*</span>
            <input type="text" name="PostalCode" class="form-control" id="PostalCode" value="{{ $customer->PostalCode }}"/>
        </div>
         <div class="form-group">
            <label for="Phone">Phone:</label> 
            <input type="text" name="Phone" class="form-control" id="Phone" value="{{ $customer->Phone }}"/>
        </div>
         <div class="form-group">
            <label for="Mobile">Mobile:</label> 
            <input type="text" name="Mobile" class="form-control" id="Mobile" value="{{ $customer->Mobile }}"/>
        </div>
        <div class="form-group">
            <label for="country_id">Country:</label>
            <select name="country_id" id="country_id" class="form-control">
            @foreach ($countrys as $country) 
                @if ($country->Name != '')
                  <option value="{{ $country->id }}">{{ $country->Name }}</option>
                @endif
            @endforeach
            </select>
        </div>
            </form>
    </div>
    
</div>
    
<div class="col-md-4">
    <h3>Customers</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>LastName</th>
                <th>FirstName</th>
            </thead>
            <tbody>
                @foreach ($customers as $cust)
                    <tr>
                        <td><a href="{{ action('CustomerController@show', $cust) }}">Select</a></td>
                        <td>{{ $cust->LastName }}</td>
                        <td>{{ $cust->FirstName }}</td>
                    </tr>
                @endforeach
                
                 {{ $customers->links() }}
            </tbody>
        </table>
    </div>
</div>

<script>
    var sel = document.getElementById('country_id');
    sel.value = {{ $customer->country->id }};
</script>

@endsection
@extends('layouts.master')
@section('title', 'Customer')

@section('content')

<div class="col-lg-8">
    
    <div class="col-lg-12 row">
            <h2 style="float:left;" class="hidden-xs">View Customer</h2>
            <form method="POST" action="{{ action('CustomerController@destroy', $customer->id) }}">
                <input type="hidden" name="_method" value="DELETE"/>    
                {{ csrf_field() }}
                <a style="float:right; margin-top: 17px;" class="btn btn-primary" href="{{ action('CustomerController@edit', $customer)  }}">Edit</a>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CustomerController@create')  }}">Insert</a>
                <button class="btn btn-danger" style="float:right; margin-top: 17px; margin-right: 5px;">Delete Customer</button>
                <a style="float:right; margin-top: 17px; margin-right: 5px;" class="btn btn-primary" href="{{ action('CustomerController@index')  }}">Cancel</a>
            </form>
    
          
        <div class="form-group" style="clear:both;">
            <label for="NickName">NickName:</label> 
            <input type="text" name="NickName" class="form-control" id="NickName" value="{{ $customer->NickName }}" disabled/>
        </div>
        <div class="form-group">
            <label for="FirstName">FirstName:</label> 
            <input type="text" name="FirstName" class="form-control" id="FirstName" value="{{ $customer->FirstName }}" disabled/>
        </div>
        <div class="form-group">
            <label for="LastName">LastName:</label> 
            <input type="text" name="LastName" class="form-control" id="LastName" value="{{ $customer->LastName }}" disabled/>
        </div>
        <div class="form-group">
            <label for="Address1">Address1:</label> 
            <input type="text" name="Address1" class="form-control" id="Address1" value="{{ $customer->Address1 }}" disabled/>
        </div>
        <div class="form-group">
            <label for="Address2">Address2:</label> 
            <input type="text" name="Address2" class="form-control" id="Address2" value="{{ $customer->Address2 }}" disabled/>
        </div>
         <div class="form-group">
            <label for="City">City:</label> 
            <input type="text" name="City" class="form-control" id="City" value="{{ $customer->City }}" disabled/>
        </div>
         <div class="form-group">
            <label for="Region">Region:</label> 
            <input type="text" name="Region" class="form-control" id="Region" value="{{ $customer->Region }}" disabled/>
        </div>
         <div class="form-group">
            <label for="PostalCode">PostalCode:</label> 
            <input type="text" name="PostalCode" class="form-control" id="PostalCode" value="{{ $customer->PostalCode }}" disabled/>
        </div>
         <div class="form-group">
            <label for="Phone">Phone:</label> 
            <input type="text" name="Phone" class="form-control" id="Phone" value="{{ $customer->Phone }}" disabled/>
        </div>
         <div class="form-group">
            <label for="Mobile">Mobile:</label> 
            <input type="text" name="Mobile" class="form-control" id="Mobile" value="{{ $customer->Mobile }}" disabled/>
        </div>
        <div class="form-group">
            <label for="country_id">Country:</label>
            <select name="country_id" id="country_id" class="form-control" disabled>
            @foreach ($countrys as $country) 
             
                  <option value="{{ $country->id }}">{{ $country->Name }}</option>
               
            @endforeach
            </select>
        </div>
            
            
        
    </div>
    
</div>

<div class="col-md-4">
    <h3>Customers</h3>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Select</th>
                <th>LastName</th>
                <th>FirstName</th>
            </thead>
            <tbody>
                @foreach ($customers as $cust)
                    <tr>
                        <td><a href="{{ action('CustomerController@show', $cust) }}">Select</a></td>
                        <td>{{ $cust->LastName }}</td>
                        <td>{{ $cust->FirstName }}</td>
                    </tr>
                @endforeach
                
                 {{ $customers->links() }}
            </tbody>
        </table>
    </div>
</div>


<script>
    var sel = document.getElementById('country_id');
    sel.value = {{ $customer->country->id }};
</script>

@endsection
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('NickName', 10);
            $table->string('FirstName', 255);
            $table->string('LastName', 255);
            $table->string('Address1', 255);
            $table->string('Address2', 255)->nullable();
            $table->string('City', 255);
            $table->string('Region', 80)->nullable();
            $table->string('PostalCode', 20);
            $table->string('Phone', 40)->nullable();
            $table->string('Mobile', 40)->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

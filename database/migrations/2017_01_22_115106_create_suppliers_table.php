<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Code', 10)->unique();
            $table->string('Name', 255)->unique();
            $table->string('Contact', 255)->nullable();
            $table->string('Address', 255)->nullable();
            $table->string('City', 255)->nullable();
            $table->string('Region', 80)->nullable();
            $table->string('PostalCode', 20)->nullable()->charset('latin1');
            $table->string('Phone', 40)->nullable()->charset('latin1');
            $table->string('Mobile', 40)->nullable()->charset('latin1');
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}

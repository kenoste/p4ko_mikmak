<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MySeeder::class);
    }
}

use App\Country;
use App\Customer;
use App\Supplier;

class MySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = new Country;
        $country->Code = 'XX';
        $country->Name = 'Unknown';
        $country->save();
        
        $country = new Country;
        $country->Code = 'BE';
        $country->Name = 'Belgie';
        $country->save();
        
        $country = new Country;
        $country->Code = 'NL';
        $country->Name = 'Nederland';
        $country->save();
        
        $country = new Country;
        $country->Code = 'GB';
        $country->Name = 'Engeland';
        $country->save();
        
        $country = new Country;
        $country->Code = 'IT';
        $country->Name = 'Italie';
        $country->save();
        
        $country = new Country;
        $country->Code = 'BS';
        $country->Name = 'Bahamas';
        $country->save();
        
        $country = new Country;
        $country->Code = 'KH';
        $country->Name = 'Cambodia';
        $country->save();
        
        
        
        $customer = new Customer;
        $customer->NickName = 'override';
        $customer->FirstName = 'Ken';
        $customer->LastName = 'Oste';
        $customer->Address1 = 'vleer';
        $customer->City = 'deurne';
        $customer->PostalCode = '2100';
        $customer->country_id = '2'; 
        $customer->save();
        
        $customer = new Customer;
        $customer->NickName = 'override2';
        $customer->FirstName = 'Ken2';
        $customer->LastName = 'Oste2';
        $customer->Address1 = 'vleer2';
        $customer->City = 'deurne2';
        $customer->PostalCode = '2100';
        $customer->country_id = '3'; 
        $customer->save();
        
        $customer = new Customer;
        $customer->NickName = 'override3';
        $customer->FirstName = 'Ken3';
        $customer->LastName = 'Oste3';
        $customer->Address1 = 'vleer2';
        $customer->City = 'deurne2';
        $customer->PostalCode = '2100';
        $customer->country_id = '3'; 
        $customer->save();
        
        $customer = new Customer;
        $customer->NickName = 'override4';
        $customer->FirstName = 'Ken4';
        $customer->LastName = 'Oste4';
        $customer->Address1 = 'vleer2';
        $customer->City = 'deurne2';
        $customer->PostalCode = '2100';
        $customer->country_id = '3'; 
        $customer->save();
        
        $customer = new Customer;
        $customer->NickName = 'override5';
        $customer->FirstName = 'Ken5';
        $customer->LastName = 'Oste5';
        $customer->Address1 = 'vleer2';
        $customer->City = 'deurne2';
        $customer->PostalCode = '2100';
        $customer->country_id = '3'; 
        $customer->save();
        
        $customer = new Customer;
        $customer->NickName = 'override6';
        $customer->FirstName = 'Ken6';
        $customer->LastName = 'Oste6';
        $customer->Address1 = 'vleer2';
        $customer->City = 'deurne2';
        $customer->PostalCode = '2100';
        $customer->country_id = '3'; 
        $customer->save();
        
        $customer = new Customer;
        $customer->NickName = 'override7';
        $customer->FirstName = 'Ken7';
        $customer->LastName = 'Oste7';
        $customer->Address1 = 'vleer2';
        $customer->City = 'deurne2';
        $customer->PostalCode = '2100';
        $customer->country_id = '3'; 
        $customer->save();
        
        $supplier = new Supplier;
        $supplier->Code = '004';
        $supplier->Name = 'google';
        $supplier->country_id = '3';
        $supplier->save();
        
        $supplier = new Supplier;
        $supplier->Code = '005';
        $supplier->Name = 'Telenet';
        $supplier->country_id = '2';
        $supplier->save();
        
        $supplier = new Supplier;
        $supplier->Code = '006';
        $supplier->Name = 'Bedrijf6';
        $supplier->country_id = '2';
        $supplier->save();
        
        $supplier = new Supplier;
        $supplier->Code = '007';
        $supplier->Name = 'Bedrijf7';
        $supplier->country_id = '2';
        $supplier->save();
        
        $supplier = new Supplier;
        $supplier->Code = '008';
        $supplier->Name = 'Bedrijf8';
        $supplier->country_id = '2';
        $supplier->save();
        
        $supplier = new Supplier;
        $supplier->Code = '009';
        $supplier->Name = 'Bedrijf9';
        $supplier->country_id = '2';
        $supplier->save();
        
        
    }
}
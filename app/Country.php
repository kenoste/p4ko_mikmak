<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
     protected $fillable = [
        'Code',
        'Longitude',
        'Latitude',
        'Name',
        'ShippingCostMultiplier',
       
        
    ];
    
    
   
}

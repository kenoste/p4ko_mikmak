<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Customer;
use Illuminate\Support\Facades\DB;



class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countrys = Country::orderBy('Name', 'ASC')->paginate(5);
        return view('country.index', ['countrys' => $countrys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrys = Country::orderBy('Name', 'ASC')->paginate(5);
        return view('country.create', ['countrys' => $countrys]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $this->validate($request, [
            'Code' => 'required|max:2|min:2|unique:countries',
            'Name' => 'required|max:255|unique:countries',
            'Latitude' => 'regex:/^-?\d*(\.\d+)?$/',
            'Longitude' => 'regex:/^-?\d*(\.\d+)?$/',
            'ShippingCostMultiplier' => 'regex:/^-?\d*(\.\d+)?$/',
        ]);
        
       
        $country = new Country;
        $country->Code = $request->input('Code');
        $country->Name = $request->input('Name');
        
        if ($request->input('Latitude') != '')
            $country->Latitude = $request->input('Latitude');
        if ($request->input('Longitude') != '')
            $country->Longitude = $request->input('Longitude');
        if ($request->input('ShippingCostMultiplier') != "")
            $country->ShippingCostMultiplier = $request->input('ShippingCostMultiplier');
       
        $country->save();
        
        return redirect('/country');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::find($id);
        $countrys = Country::orderBy('Name', 'ASC')->paginate(5);
        return view('country.show', ['countrys' => $countrys, 'country' => $country]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        $countrys = Country::orderBy('Name', 'ASC')->paginate(5);
        return view('country.edit', ['country' => $country, 'countrys' => $countrys ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         if ($id != 1) {
         
            $country = Country::find($id);
            
            $codeValidator = "required|max:2|min:2";
            if ( strtoupper($country->Code) != strtoupper($request->input('Code')) ){
                $codeValidator = $codeValidator . "|unique:countries";
            }
            
            $NameValidator = "required|max:255";
            if ($country->Name != $request->input('Name')){
                $NameValidator = $NameValidator . "|unique:countries";
            }
            
           $this->validate($request, [
                'Code' => $codeValidator,
                'Name' => $NameValidator,
                'Latitude' => 'regex:/^-?\d*(\.\d+)?$/',
                'Longitude' => 'regex:/^-?\d*(\.\d+)?$/',
                'ShippingCostMultiplier' => 'regex:/^-?\d*(\.\d+)?$/',
            ]);
           
          
            $country->Code = $request->input('Code');
            $country->Name = $request->input('Name');
            
            if ($request->input('Latitude') != '')
                $country->Latitude = $request->input('Latitude');
            if ($request->input('Longitude') != '')
                $country->Longitude = $request->input('Longitude');
            if ($request->input('ShippingCostMultiplier') != "")
                $country->ShippingCostMultiplier = $request->input('ShippingCostMultiplier');
         }
       
       $country->save();
       
        return redirect('/country');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        if ($id != 1)
        {
             DB::table('customers')
            ->where('country_id', $id)
            ->update(['country_id' => 1]);
            
             DB::table('suppliers')
            ->where('country_id', $id)
            ->update(['country_id' => 1]);
        
            $country = Country::find($id);
            $country->delete();   
        }
        
        return redirect('/country');
        
    }
}

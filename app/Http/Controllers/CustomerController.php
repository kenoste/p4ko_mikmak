<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Country;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderBy('LastName', 'ASC')->paginate(5);
        return view('customer.index', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::orderBy('LastName', 'ASC')->paginate(5);
        $countrys = Country::orderBy('Name', 'ASC')->get();
        return view('customer.create', ['customers' => $customers, 'countrys' => $countrys ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'NickName' => 'required|max:10',
            'FirstName' => 'required|max:255',
            'LastName' => 'required|max:255',
            'Address1' => 'required|max:255',
            'Address2' => 'max:255',
            'City' => 'required|max:255',
            'Region' => 'max:80',
            'PostalCode' => 'required|max:20',
            'Phone' => 'max:40',
            'Mobile' => 'max:40',
        ]);
        
        
        $customer = new Customer;
        $customer->NickName = $request->input('NickName');
        $customer->FirstName = $request->input('FirstName');
        $customer->LastName = $request->input('LastName');
        $customer->Address1 = $request->input('Address1');
        $customer->City = $request->input('City');
        $customer->PostalCode = $request->input('PostalCode');
        $customer->country_id = $request->input('country_id');
        
        if ($request->input('Address2') != '' )
            $customer->Address2 = $request->input('Address2');
        if ($request->input('Region') != '' )
            $customer->Region = $request->input('Region');
        if ($request->input('Phone') != '')
            $customer->Phone = $request->input('Phone');
        if ($request->input('Mobile') != '')  
            $customer->Mobile = $request->input('Mobile');
            
        $customer->save();
        
        
        return redirect('/customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        $customers = Customer::orderBy('LastName', 'ASC')->paginate(5);
        $countrys = Country::orderBy('Name', 'ASC')->get();
        return view('customer.show', ['customer' => $customer, 'customers' => $customers, 'countrys' => $countrys ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countrys = Country::orderBy('Name', 'ASC')->get();
        $customer = Customer::find($id);
        $customers = Customer::orderBy('LastName', 'ASC')->paginate(5);
        return view('customer.edit', ['customer' => $customer, 'customers' => $customers, 'countrys' => $countrys ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'NickName' => 'required|max:10',
            'FirstName' => 'required|max:255',
            'LastName' => 'required|max:255',
            'Address1' => 'required|max:255',
            'Address2' => 'max:255',
            'City' => 'required|max:255',
            'Region' => 'max:80',
            'PostalCode' => 'required|max:20',
            'Phone' => 'max:40',
            'Mobile' => 'max:40',
        ]);
        
        
        $customer = Customer::find($id);
        $customer->NickName = $request->input('NickName');
        $customer->FirstName = $request->input('FirstName');
        $customer->LastName = $request->input('LastName');
        $customer->Address1 = $request->input('Address1');
        $customer->City = $request->input('City');
        $customer->PostalCode = $request->input('PostalCode');
        $customer->country_id = $request->input('country_id');
        
        if ($request->input('Address2') != '' )
            $customer->Address2 = $request->input('Address2');
        if ($request->input('Region') != '' )
            $customer->Region = $request->input('Region');
        if ($request->input('Phone') != '')
            $customer->Phone = $request->input('Phone');
        if ($request->input('Mobile') != '')  
            $customer->Mobile = $request->input('Mobile');
        
        
        
        $customer->save();
        
        return redirect('/customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete(); 
        
        return redirect('/customer');
    }
}

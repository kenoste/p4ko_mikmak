<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\Country;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::orderBy('Name', 'ASC')->paginate(5);
        return view('supplier.index', ['suppliers' => $suppliers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::orderBy('Name', 'ASC')->paginate(5);
        $countrys = Country::orderBy('Name', 'ASC')->get();
        return view('supplier.create', ['suppliers' => $suppliers, 'countrys' => $countrys ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'Code' => 'required|max:10|unique:suppliers',
            'Name' => 'required|max:255|unique:suppliers',
            'Contact' => 'max:255',
            'Address' => 'max:255',
            'City' => 'max:255',
            'Region' => 'max:80',
            'PostalCode' => 'max:20|regex:/^[a-zA-Z0-9]/',
            'Phone' => 'max:40|regex:/^[a-zA-Z0-9]/',
            'Mobile' => 'max:40|regex:/^[a-zA-Z0-9]/',
        ]);
        
        $supplier = new Supplier;
        $supplier->Code = $request->input('Code');
        $supplier->Name = $request->input('Name');
        $supplier->country_id = $request->input('country_id');
        
        if ($request->input('Contact') != '')
            $supplier->Contact = $request->input('Contact');
        if ($request->input('Address') != '')
            $supplier->Address = $request->input('Address');
        if ($request->input('City') != '')
            $supplier->City = $request->input('City');
        if ($request->input('Region') != '')
            $supplier->Region = $request->input('Region');
        if ($request->input('PostalCode') != '')
            $supplier->PostalCode = $request->input('PostalCode');
        if ($request->input('Phone') != '')
            $supplier->Phone = $request->input('Phone');
        if ($request->input('Mobile') != '')
            $supplier->Mobile = $request->input('Mobile');
        
         $supplier->save(); 
        
         return redirect('/supplier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::find($id);
        $suppliers = Supplier::orderBy('Name', 'ASC')->paginate(5);
        $countrys = Country::orderBy('Name', 'ASC')->get();
        return view('supplier.show', ['supplier' => $supplier, 'suppliers' => $suppliers, 'countrys' => $countrys ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countrys = Country::orderBy('Name', 'ASC')->get();
        $supplier = Supplier::find($id);
        $suppliers = Supplier::orderBy('Name', 'ASC')->paginate(5);
        return view('supplier.edit', ['supplier' => $supplier, 'suppliers' => $suppliers, 'countrys' => $countrys ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);
        
        $codeValidator = "required|max:10";
        if ( strtoupper($supplier->Code) != strtoupper($request->input('Code'))){
            $codeValidator = $codeValidator . "|unique:suppliers";
        }
        
        $NameValidator = "required|max:255";
        if ($supplier->Name != $request->input('Name')){
            $NameValidator = $NameValidator . "|unique:suppliers";
        }
        
       $this->validate($request, [
            'Code' => $codeValidator,
            'Name' => $NameValidator,
            'Contact' => 'max:255',
            'Address' => 'max:255',
            'City' => 'max:255',
            'Region' => 'max:80',
            'PostalCode' => 'regex:/^[a-zA-Z0-9]/|max:20',
            'Phone' => 'max:40|regex:/^[a-zA-Z0-9]/',
            'Mobile' => 'max:40|regex:/^[a-zA-Z0-9]/',
        ]);
        
        
        $supplier->Code = $request->input('Code');
        $supplier->Name = $request->input('Name');
        $supplier->country_id = $request->input('country_id');
        
        if ($request->input('Contact') != '')
            $supplier->Contact = $request->input('Contact');
        if ($request->input('Address') != '')
            $supplier->Address = $request->input('Address');
        if ($request->input('City') != '')
            $supplier->City = $request->input('City');
        if ($request->input('Region') != '')
            $supplier->Region = $request->input('Region');
        if ($request->input('PostalCode') != '')
            $supplier->PostalCode = $request->input('PostalCode');
        if ($request->input('Phone') != '')
            $supplier->Phone = $request->input('Phone');
        if ($request->input('Mobile') != '')
            $supplier->Mobile = $request->input('Mobile');
        
         $supplier->save(); 
         
         return redirect('/supplier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        $supplier->delete(); 
        
        return redirect('/supplier');
    }
}

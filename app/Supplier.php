<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Country;

class Supplier extends Model
{
    public function country()
    {
        return $this->belongsTo('App\Country');
      
    }
}
